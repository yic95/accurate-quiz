# Accurate Quiz

要求精確答案的小考，適用單字考試

# 執行期依賴

- python3

# 使用

## 啟動

```
$ python3 main.py <題庫名稱> [flag]
```
### flag

* -loop -l: loop mode
* -rand -r: random mode (default)

## 答題

啟動之後，你應該會看到類似這樣的畫面：

```
1 蘋果: 
```
輸入答案，按下 `Enter`

如果答對，則會直接進入下一題。
如果答錯，你會看到以下畫面：

```
1 蘋果: banana
banana -> apple
2 芭樂: 
```

## 離開

作答空白（直接按`Enter`），會出現以下畫面：

```
1 蘋果: 
press enter to exit 
```

如果直接按`Enter`，離開程式，
你會看到以下畫面：

```
press enter to exit
Accuracy: 0.0%
```

accuracy 指的是答題精準度

# 新增題庫

題目都應該放在`<家目錄>/.voca`裡面，檔案名稱為`<題庫名稱>.json`，格式如下：

```json
{
    "題目":"答案",
    "題目2":"答案2"
}
```

注意：題目不能重複

# Known Issues

- Won't run on MS Windows. Traceback message:

```
Traceback (most recent call last):
  File "C:\Users\USER\source\accurate-quiz\main.py", line 156, in <module>
    main()
  File "C:\Users\USER\source\accurate-quiz\main.py", line 130, in main
    for k, v in json.load(f).items():
  File "C:\Users\USER\AppData\Local\Programs\Python\Python39\lib\json\__init__.py", line 293, in load
    return loads(fp.read(),
UnicodeDecodeError: 'cp950' codec can't decode byte 0x80 in position 11: illegal multibyte sequence
```

# TO-DO

- [X] 新增其他考試模式
- [ ] 新增 loop mode 的文件
- [ ] 新增分析模式
