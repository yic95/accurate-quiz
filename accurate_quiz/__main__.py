#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Vocabulary Test Program Written in Python by Yic95."""


import os
import sys
import json
import random
import logging


PATH_SEP = os.path.sep
HOME = os.path.expandvars('$HOME')


def quiz_core(ans: str, prompt: str) -> int:
    """
    ans: standerd answer.
    RETURN VALUE:
    0  answer correct
    1  answer wrong
    2  exit.
    3  exit but canceled"""
    inp = input(prompt)
    if inp == '':
        if input("press enter to exit ") == '':
            return 2
        return 3
    if inp != ans:
        print("\033[31m{} -> {}\033[0m".format(inp, ans))
        return 1
    return 0


def rand_mode_loop(voc_dict: dict) -> dict:
    """random mode quiz.  return (question count: interger, accuracy_float_point: 0~1)"""
    voc_dict_arr = list(voc_dict.items())
    quecount = 0
    ans_iacc_count = 0
    while True:
        rnd = random.randint(0, len(voc_dict_arr)-1)
        que, ans = voc_dict_arr[rnd]

        question_exit_value = quiz_core(
            ans, '{} {}: '.format(int(quecount+1), que))
        if question_exit_value == 1:
            ans_iacc_count += 1
        elif question_exit_value == 2:
            break
        elif question_exit_value == 3:
            continue

        quecount += 1
    return {'q': quecount, 'a': quecount - ans_iacc_count}


def loop_mode_quiz(voc_dict: dict, randomize: bool=False) -> dict:
    voc_dict_arr = list(voc_dict.items())
    if randomize:
        random.shuffle(voc_dict_arr)
    quecount = 0
    ans_iacc_count = 0
    is_exit = True
    round_count = 1
    que_round_count = 1

    while True:
        is_exit = True
        que_round_count = 1
        if randomize:
            random.shuffle(voc_dict_arr)
        for j in voc_dict_arr:
            que, ans = j
            question_exit_value = quiz_core(
                ans, f'{round_count}.{que_round_count} {que}: ')

            if question_exit_value == 1:
                is_exit = False
                ans_iacc_count += 1
            elif question_exit_value == 2:
                is_exit = True
                break
            elif question_exit_value == 3:
                continue
            quecount += 1
            que_round_count += 1

        if is_exit is True:
            break

        print('-------------------- {}'.format(round_count))
        round_count += 1

    return {'q': quecount, 'a': quecount - ans_iacc_count}


def end_print(info: tuple) -> None:
    """Print accuracy in the end."""
    if info['q'] == 0:
        return
    print('accuracy: {}%'.format(round(info['a'] / info['q'] * 100, 2)))


def start_print(info: dict) -> None:
    """print vocabulary file count and vocabulary count at the beginning."""
    filemsg = "file" if info['f'] == 1 else "files"
    vocabularymsg = "vocabulary" if info['v'] == 1 else "vocabularies"
    logging.info(f"{info['f']} {filemsg}, {info['v']} {vocabularymsg}")


def main() -> int:
    """main"""
    logfmt = "[%(levelname)s] %(message)s"
    logging.basicConfig(level=logging.INFO, format=logfmt)

    quiz_dict = {}
    quiz_mode = ['rand_mode']
    file_count = 0

    if not os.path.isdir('{}{}.voca'.format(HOME, PATH_SEP)):
        os.makedirs('{}{}.voca'.format(HOME, PATH_SEP))
        print("Directory '~/.voca' was created. Add json files there.")

    if len(sys.argv) < 2:
        logging.error("File name not found, exit.")
        return 1

    for i in sys.argv[1::]:  # run through all the arguments and check if it is a file
        if not i.startswith('-'):
            json_file_full_path = f'{HOME}{PATH_SEP}.voca{PATH_SEP}{i}.json'
            if os.path.isfile(json_file_full_path):
                # add file 'i' to the quiz_dict
                with open(json_file_full_path) as f:
                    for k, v in json.load(f).items():
                        quiz_dict.setdefault(k, v)
                file_count += 1
            else:
                logging.error("File {} doesn't exists. Won't be added to the quiz list."
                              .format(json_file_full_path))
        else:
            if i[1::] in ('rand_mode', 'rand', 'rm', 'r'):
                quiz_mode.append('rand_mode')
            elif i[1::] in ('loop_mode', 'loop', 'lm', 'l'):
                quiz_mode.append('loop_mode')
            elif i[1::] in ('rand_loop', 'rloop', 'rl'):
                quiz_mode.append('rand_loop')
            elif i == '--help':
                print(
                    "\n$ python3 main.py [-quiz_mode] <quiz_file> [quiz_file_2]\n")
                quiz_mode.clear()
                break

    if quiz_mode == '':
        return 1
    if len(quiz_dict) == 0:
        return 1

    start_print({'f': file_count, 'v': len(quiz_dict)})

    if ('rand_mode' in quiz_mode and 'loop_mode' in quiz_mode) or 'rand_loop' in quiz_mode:
        end_print(loop_mode_quiz(quiz_dict, True))
    elif 'rand_mode' in quiz_mode:
        end_print(rand_mode_loop(quiz_dict))
    elif 'loop_mode' in quiz_mode:
        end_print(loop_mode_quiz(quiz_dict))
    return 0


if __name__ == '__main__':
    try:
        main()
    except json.decoder.JSONDecodeError:
        print("json file syntax error.")
    except KeyboardInterrupt:
        print("\n\aPress ENTER twice to exit.")
        quit()
    except EOFError:
        print("\n\aPress ENTER twice to exit.")
        quit()
